import { ApiProperty } from '@nestjs/swagger';

export class GetStoreDto {
  @ApiProperty({ 
    required: true, 
    description: 'page of the store',
    default: 1,
  })
  page: number;

  @ApiProperty({ 
    required: true, 
    description: 'size of the store',
    default: 5,
  })
  size: number;

//   @ApiProperty({
//     required: false,
//     description: '',
//   })
//   status: string;
}
