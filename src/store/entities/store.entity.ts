import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
  } from 'typeorm';
  
  /**
   * Profile Entity Class
   */
  @Entity({ name: 'we_store_list' })
  export class StoreEntity {
    @PrimaryGeneratedColumn()
    _id: number;

    @Column()
    city: string;

    @Column()
    region: string;
    
    @Column()
    region_detail: string;
    
    @Column()
    region_road: string;
    
    @Column()
    region_road_detail: string;
    
    @Column()
    store_name: string;
    
    @Column()
    store_name_sub: string;
    
    @Column()
    food_type: string;
    
    @Column()
    region_type: string;
    
    @Column()
    main_menu: string;
   
    @Column()
    sub_menu: string;
    
    @Column()
    visitor_cnt: number;
    
    @Column()
    visitor_rate: number;
    
    @Column()
    created_at: Date;
    
    @Column()
    updated_at: Date;
    
    @Column()
    hide: number;
    
    @Column()
    hide_at: Date;
    
    @Column()
    description: string;
  }
  