import { Module } from '@nestjs/common';
import { StoreController } from './store.controller';
import { StoreService } from './store.service';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StoreEntity } from './entities/store.entity';
import { TypeOrmExModule } from './db/typeorm.module';
import { StoreRepository } from './store.repository';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([
      StoreEntity,
    ]),
    TypeOrmExModule.forCustomRepository([
      StoreRepository,
    ]),
  ],
  controllers: [StoreController],
  providers: [StoreService]
})
export class StoreModule {}
