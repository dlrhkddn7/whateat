import { Injectable, Query } from '@nestjs/common';
import { StoreRepository } from './store.repository';

@Injectable()
export class StoreService {
    constructor(
        private storeRepository: StoreRepository
    ) {}

    async getStore(options) {
        return await this.storeRepository.getStore(options);
    }

}
