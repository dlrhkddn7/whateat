import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { GetStoreDto } from './dto/getStore.dto';
import { StoreService } from './store.service';

@ApiTags('가게 정보(Store)')
@Controller('store')
export class StoreController {
    constructor(
        private storeService: StoreService,
    ){
        this.storeService = storeService;
    }

    @Get('/list')
    async getStore(@Query() query: GetStoreDto) {
        const page = (query.page - 1) * query.size;
        const size = query.size;

        let options;
        return await this.storeService.getStore({
            page: page,
            size: size,
        });
    }

}
