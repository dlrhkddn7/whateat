import { Repository } from "typeorm";
import { CustomRepository } from "./db/what-eat.decorator";
import { StoreEntity } from "./entities/store.entity";

@CustomRepository(StoreEntity)
export class StoreRepository extends Repository<StoreEntity> {
    async getStore(options) {
        const result = await this.query(
            `
            SELECT * FROM we_store_list
            LIMIT ?,?
            `,[
                parseInt(options.page), 
                parseInt(options.size)
            ],
        )
        return result
    }
}
