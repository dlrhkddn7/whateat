import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MyLocateEntity } from './entities/my-locate.entity';
import { TypeOrmExModule } from './db/typeorm.module';
import { MyLocateRepository } from './my-locate.repository';
import { MyLocateController } from './my-locate.controller';
import { MyLocateService } from './my-locate.service';

@Module({
    imports: [
        HttpModule,
        TypeOrmModule.forFeature([
            MyLocateEntity,
        ]),
        TypeOrmExModule.forCustomRepository([
            MyLocateRepository,
        ]),
    ],
    controllers: [MyLocateController],
    providers: [MyLocateService],
    exports: [MyLocateService],
})
export class MyLocateModule {}
