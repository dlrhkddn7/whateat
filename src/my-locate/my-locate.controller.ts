import { Get } from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { MyLocateService } from './my-locate.service';

@Controller('my-locate')
export class MyLocateController {
    constructor(
        private myLocateService: MyLocateService,
    ) {
        this.myLocateService = myLocateService;
    }

    @Get('test') 
    async getMyLocate() {
        return await this.myLocateService.getMyLocate();
    }
}
