import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
  } from 'typeorm';
  
  /**
   * Profile Entity Class
   */
  @Entity({ name: 'test_db' })
  export class MyLocateEntity {
    @PrimaryGeneratedColumn()
    _id: number;

    @Column()
    user_name: string;

    @Column()
    user_id: string;
  }
  