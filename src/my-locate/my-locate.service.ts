import { Injectable } from '@nestjs/common';
import { MyLocateRepository } from './my-locate.repository';

@Injectable()
export class MyLocateService {
    constructor(
        private myLocateRepositoy: MyLocateRepository,
    ) {}

    async getMyLocate() {
        let res = await this.myLocateRepositoy.getData();
        console.log(res);
        return res
    }
}
