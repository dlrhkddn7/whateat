import { Repository } from "typeorm";
import { CustomRepository } from "./db/what-eat.decorator";
import { MyLocateEntity } from "./entities/my-locate.entity";

@CustomRepository(MyLocateEntity)
export class MyLocateRepository extends Repository<MyLocateEntity> {
    async getData() {
        const result = await this.query(
            `
            SELECT * FROM test_db
            LIMIT 0, 10
            `
        )
        return result
    }
}
