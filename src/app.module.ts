import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MyLocateModule } from './my-locate/my-locate.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StoreModule } from './store/store.module';
import { UserModule } from './user/user.module';
import { UserEntity } from './user/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'www.devkids.co.kr',
      port: 14402,
      username: 'newming',
      password: 'u8Yto93qrAgW',
      charset: 'utf8mb4',
      timezone: 'utc',
      database: 'whateat',
      entities: [__dirname + '/entities/*.entity.{ts,js}', UserEntity]
      // entities: ['src/geeknews/*.repository.ts'],
    }),
    MyLocateModule,
    StoreModule,
    UserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
