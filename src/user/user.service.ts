import { Injectable } from '@nestjs/common';
import { insertUserDto } from './dto/insertUser.dto';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
    constructor(
        private userRepository: UserRepository,
    ) {}

    async getUser(options) {
        let items = await this.userRepository.getUser(options);
        let result = []
        for( let item of items ) {
            item = await this.map(item);
            result.push(item);
        }

        return result;
    }

    async insertUser(dto: insertUserDto) {
        return await this.userRepository.insertUser(dto);
    }

    async map(data) {
        let res
        res = {
            _id: data._id,
            user_name: data.user_name,
            user_region: data.user_region.split(','),
            user_food_type: data.user_food_type,
            user_bookmark_store: data.user_bookmark_store.split(','),
            user_type: data.user_type,
            hide: data.hide,
        }

        return res;
    }
}
