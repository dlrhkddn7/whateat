import { ApiProperty } from '@nestjs/swagger';

export class GetUserDto {
  @ApiProperty({ 
    required: true, 
    description: 'page of the user',
    default: 1,
  })
  page: number;

  @ApiProperty({ 
    required: true, 
    description: 'size of the user',
    default: 5,
  })
  size: number;

//   @ApiProperty({
//     required: false,
//     description: '',
//   })
//   status: string;
}
