import { ApiProperty } from '@nestjs/swagger';

export class insertUserDto {
  @ApiProperty({ 
    required: true, 
    description: 'user_name',
  })
  user_name: string;

  @ApiProperty({ 
    required: true, 
    description: '유저 선호 지역',
    default: '',
  })
  user_region: string;

  @ApiProperty({ 
    required: true, 
    description: '유저 선호 음식',
    default: '',
  })
  user_food_type: string;

  @ApiProperty({ 
    required: false, 
    description: '유저 선호 음식',
    default: '',
  })
  user_bookmark_store: string;

  @ApiProperty({
    required: true,
    description: '유저 타입 0 : admin, 1 : guest, 2 : user',
    enum: [0, 1, 2],
    default: 1,
  })
  user_type: number;
}
