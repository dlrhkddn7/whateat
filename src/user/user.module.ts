import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmExModule } from './db/typeorm.module';
import { UserEntity } from './entities/user.entity';
import { UserController } from './user.controller';
import { UserRepository } from './user.repository';
import { UserService } from './user.service';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([
      UserEntity,
    ]),
    TypeOrmExModule.forCustomRepository([
      UserRepository,
    ])
  ],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {}
