import { Repository } from "typeorm";
import { CustomRepository } from "./db/what-eat.decorator";
import { UserEntity } from "./entities/user.entity";

@CustomRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {
    async getUser(options) {
        const result = await this.query(
            `
            SELECT * FROM we_user
            LIMIT ?,?
            `,[
                parseInt(options.page), 
                parseInt(options.size)
            ],
        )
        return result
    }

    async insertUser(options) {
        const res = {
            user_name: options.user_name,
            user_region: options.user_region,
            user_food_type: options.user_food_type,
            user_bookmark_store: options.user_bookmark_store,
            user_type: options.user_type,
        }
        await this.insert(res);
        return res;
    }
}
