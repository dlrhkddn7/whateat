import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
  } from 'typeorm';
  
  /**
   * Profile Entity Class
   */
  @Entity({ name: 'we_user' })
  export class UserEntity {
    @PrimaryGeneratedColumn()
    _id: number;

    @Column()
    user_name: string;

    @Column()
    user_region: string;
    
    @Column()
    user_food_type: string;
    
    @Column()
    user_bookmark_store: string;
    
    @Column()
    user_type: number;
    
    @Column()
    created_at: Date;
    
    @Column()
    updated_at: Date;
    
    @Column()
    hide: number;
    
    @Column()
    hide_at: Date;
    
    @Column()
    description: string;
  }
  