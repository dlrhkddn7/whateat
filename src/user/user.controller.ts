import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { GetUserDto } from './dto/getUser.dto';
import { insertUserDto } from './dto/insertUser.dto';
import { UserService } from './user.service';

@ApiTags('유저 정보(User)')
@Controller('user')
export class UserController {
    constructor(
        private userService: UserService,
    ) {
        this.userService = userService;
    }

    @Post('/insert')
    async insertUser(@Body() insertUserDto: insertUserDto) {
        const res = await this.userService.insertUser(insertUserDto);
        return res;
    }

    @Get('/list')
    async getUser(@Query() query: GetUserDto) {
        const page = (query.page - 1) * query.size;
        const size = query.size;

        let options;
        return await this.userService.getUser({
            page: page,
            size: size,
        });
    }
}
